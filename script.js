var j = 0 //usada para contar o número de questões
var o = 0 //usada para contar o item 
var respostas0 = [4] //foi criado um vetor de itens para cada questão
var respostas1 = [4]
var respostas2 = [4]
var respostas3 = [4]
var respostas4 = [4]
var perguntas = [5]

var ajax = new XMLHttpRequest();
ajax.open('GET', 'https://quiz-trainee.herokuapp.com/questions');
ajax.onreadystatechange = function () {
    if (ajax.status === 200 && ajax.readyState === 4) {
        var res = JSON.parse(ajax.responseText);
        console.log(res.length)

        perguntas.length = res.length //igualando o comprimento do vetor perguntas ao da api

        for (o = 0; o < res[j].options.length; o++) {
            respostas0[o] = res[0].options[o].answer //igualando o vetor dos itens de cada questão ao dos da api 
        }
        for (o = 0; o < res[j].options.length; o++) {
            respostas1[o] = res[1].options[o].answer
        }
        for (o = 0; o < res[j].options.length; o++) {
            respostas2[o] = res[2].options[o].answer
        }
        for (o = 0; o < res[j].options.length; o++) {
            respostas3[o] = res[3].options[o].answer
        }
        for (o = 0; o < res[j].options.length; o++) {
            respostas4[o] = res[4].options[o].answer
        }
        for (j = 0; j < res.length; j++) {
            perguntas[j] = res[j].title
        }
    } else {
        console.log(ajax.status) //caso algo dê errado na requisição de dados da api, ela apontatará no console o status
    }
}
ajax.send();

var pontos = 0 //usada para contabilizar os pontos do "jogador"
var p = 0 //usada para contabilizara questão em que o jogador está e que tela deve estar visível para ele
var itens = document.getElementsByClassName("resposta") //itens representa a "tela" em que aparecem os itens
var itemmarc = document.getElementsByName("item") // itemmarc é o vetor que corresponde a cada item individualmente do HTML
var questao = document.getElementById("questao")

document.getElementById("proximo").onclick = function () { //ONDE TODA A MÁGICA ACONTECE!(Aqui é onde entra toda a lógica para fazer com que o usuário vá de uma tela para outra, contabilizar os pontos e etc...)
    document.getElementById("inicio").style.display = "none"
    document.getElementById("valor").style.display = "block"
    document.getElementById("itens").style.display = "block"
    document.getElementById("quiz").style.marginTop = "-5%"
    document.getElementById("proximo").style.marginBottom = "-3%"
    document.getElementById("proximo").style.marginTop = "20px"
    document.getElementById("secao").style.marginTop = "0%"
    document.getElementById("proximo").innerHTML = "Próximo"
    if (p > 5) {//se p for maior do que 5, significa que o usuário clicou em "recomeçar"
        p = 0 //zerando o número da pergunta
        pontos = 0 //zerando o número de pontos
        document.getElementById("final").style.display = "none"
        document.getElementById("valor").innerHTML = perguntas[p]
    }
    if (p < perguntas.length) { //ele acabou de clicar em iniciar ou em recomeçar
        document.getElementById("questao").style.display = "block"
        if (p === 0) { //pergunta 1
            itens[0].innerHTML = respostas0[0] //igualando os itens do HTML aos itens da API
            itens[1].innerHTML = respostas0[1]
            itens[2].innerHTML = respostas0[2]
            itens[3].innerHTML = respostas0[3]
            for (var i = 0; i < itens.length; i++) { //passando item a item 
                if (itemmarc[i].checked) { //procurando pelo item marcado
                    if (i == 2) { //checando se o item marcado é o certo
                        pontos++
                    }
                    p++
                }
                itemmarc[i].checked = false //desmarcando todos itens, para que o item não continue marcado após o fim da questão
            }
        }
        if (p === 1) { //o mesmo se repete para as questões a baixo
            itens[0].innerHTML = respostas1[0]
            itens[1].innerHTML = respostas1[1]
            itens[2].innerHTML = respostas1[2]
            itens[3].innerHTML = respostas1[3]
            for (var i = 0; i < itens.length; i++) {
                if (itemmarc[i].checked) {
                    if (i == 2) {
                        pontos++
                    }
                    p++
                }
                itemmarc[i].checked = false
            }
        }
        if (p === 2) {
            itens[0].innerHTML = respostas2[0]
            itens[1].innerHTML = respostas2[1]
            itens[2].innerHTML = respostas2[2]
            itens[3].innerHTML = respostas2[3]
            for (var i = 0; i < itens.length; i++) {
                if (itemmarc[i].checked) {
                    if (i == 1) {
                        pontos++
                    }
                    p++
                }
                itemmarc[i].checked = false
            }
        } if (p === 3) {
            itens[0].innerHTML = respostas3[0]
            itens[1].innerHTML = respostas3[1]
            itens[2].innerHTML = respostas3[2]
            itens[3].innerHTML = respostas3[3]
            for (var i = 0; i < itens.length; i++) {
                if (itemmarc[i].checked) {
                    if (i == 3) {
                        pontos++
                    }
                    p++
                }
                itemmarc[i].checked = false
            }
        }
        if (p === 4) {
            itens[0].innerHTML = respostas4[0]
            itens[1].innerHTML = respostas4[1]
            itens[2].innerHTML = respostas4[2]
            itens[3].innerHTML = respostas4[3]
            for (var i = 0; i < itens.length; i++) {
                if (itemmarc[i].checked) {
                    if (i == 0) {
                        pontos++
                    }
                    p++
                }
                itemmarc[i].checked = false
            }

        }
        questao.innerHTML = p + "/" + perguntas.length
        document.getElementById("valor").innerHTML = perguntas[p] //usado para mudar o valor que aparece na tela no fim de cada questão
    } if (p === perguntas.length) { //o usuário acabou todas as questões
        document.getElementById("valor").style.display = "none"
        document.getElementById("itens").style.display = "none"
        document.getElementById("final").style.display = "block"
        var nota = (100 * pontos) / perguntas.length //cálculo da nota do usuário
        if (nota === 100) { //abaixo temos uma frase diferente dependendo da nota adquirida

            document.getElementById("classification").innerHTML = "Parabéns você é um astronauta Nato!"
        } else {
            if (nota >= 70 && nota < 100) {
                document.getElementById("classification").innerHTML = "Você pode melhorar mas certamente possui potencial para ser um excelente astronauta"
            }
            else {
                if (nota < 70 && nota > 20) {
                    document.getElementById("classification").innerHTML = "Você não é um astronalta ruim mas precisa treinar mais"
                }
                else {
                    if (nota <= 20) {
                        document.getElementById("classification").innerHTML = "Você provavelmente seria responsável pela morte de alguém da nave. Por favor, mantenha-se longe do espaço"
                    }
                }
            }
        } //arredondando a nota para duas casa decimais depois da vírgula
        if (pontos != 0) { //quando ele tenta arredondar a nota zero, da erro :/
            var notaarredondada = parseFloat(nota.toFixed(2));
            document.getElementById("percent").innerHTML = "Sua porcentagem de acerto foi de " + notaarredondada + "%"
        } else {
            document.getElementById("percent").innerHTML = "Sua porcentagem de acerto foi de " + nota + "%"
        }
        p++
        document.getElementById("proximo").innerHTML = "Recomeçar" //muda o texto do botão para recomeçar
    }
}